import numpy as np
from sklearn.decomposition import PCA

attrs = np.loadtxt(open("dataWithoutCategorical.csv","rb"),delimiter=",",skiprows=1)
# attrs = allData[:,0:191]

X = np.array(attrs)
pca = PCA(n_components=5)
pca.fit(X)
PCA(copy=True, n_components=5, whiten=False)
np.savetxt("dataWithoutCategorical_pca.csv", pca.explained_variance_ratio_, delimiter=",")