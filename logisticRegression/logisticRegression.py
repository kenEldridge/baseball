import numpy, csv, random
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing

class Result:
    def __init__(self):
        pass

def examineThresholds(y_prob, testingLabels, trainingSampleSize):
    with open('outputCurves' + str(trainingSampleSize) + '.csv', 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(['truePositiveCount', 'falsePositiveCount', 'trueNegativeCount', 'falseNegativeCount', 'recall'
                           , 'precision', 'negativePredictiveValue', 'specificity', 'falsePositiveRate', 'falseNegativeRate', 'threshold', 'accuracy'])
        bestAccuracy = 0
        bestThreshold = 0
        for threshold in numpy.arange(0,1.0001,0.0001):
            testingLabels = testingLabels > 0 
            y_pred = y_prob >= threshold
            tp_array = (testingLabels == y_pred) & (y_pred == True)
            fp_array = (testingLabels != y_pred) & (y_pred == True)
            tn_array = (testingLabels == y_pred) & (y_pred == False)
            fn_array = (testingLabels != y_pred) & (y_pred == False)
            tp = float(numpy.sum(tp_array))
            fp = float(numpy.sum(fp_array))
            tn = float(numpy.sum(tn_array))
            fn = float(numpy.sum(fn_array))
            recall = tp/(tp+fn)
            if tp + fp > 0:
                precision = tp/(tp+fp)
            else:
                precision = 1
            if tn+fn > 0:
                npv = tn/(tn+fn)
            else:
                npv = 0
            if tn + fp > 0:
                specificity = tn/(tn+fp) #true negative rate
            else:
                specificity = 0
            if fp + tn > 0:
                fpr = fp/(fp+tn) #false positive rate, (1-specificity), fall-out
            else:
                fpr = 0
            fnr = fn/(fn+tp)
            acc = (tp+tn)/(tp+fp+tn+fn)
            if acc > bestAccuracy:
                bestAccuracy = acc
                bestThreshold = threshold
            csvwriter.writerow([tp, fp, tn, fn, recall, precision, npv, specificity, fpr, fnr, bestThreshold, acc])
    return {'bestThreshold': bestThreshold}

def buildThresholdCurve(learner, trainingAtters, trainingLabels, testingAtters, testingLabels):
    y_prob = learner.fit(trainingAtters, trainingLabels).predict_proba(testingAtters)[:,1]
    return examineThresholds(y_prob, testingLabels, trainingLabels.size)

allData = numpy.loadtxt(open("../data.csv","rb"),delimiter=",",skiprows=1)

with open('learningCurve.csv', 'wb') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(['sampleProportion', 'accuracy', 'loss', 'base-loss', 'tp', 'tn', 'fp', 'fn'])
    for sampleProportion in numpy.arange(0.1,1.0,0.1):
        allDataSize = float(allData.shape[0])
        trainingSize = int( round( allDataSize * sampleProportion ) )
        remainder = allDataSize - trainingSize
        testingSize = round( remainder * .5 )
        holdoutSize = testingSize
        while (trainingSize + testingSize + holdoutSize) != allDataSize:
            holdoutSize =  holdoutSize - 1
        
                #randomize labels
#         randomLabels = numpy.empty((allData.shape[0],))
#         for index, item in enumerate(randomLabels):
#             randomLabels[index] = random.randint(0, 1)
#         allData[:,193] = randomLabels
        
        trainingPre = allData[0:trainingSize-1,:]
        trainingOrdinal = trainingPre[:,0:5]
        scaler = preprocessing.StandardScaler().fit(trainingOrdinal)
        trainingOrdinalClean = scaler.transform(trainingOrdinal)
        trainingCategorial = trainingPre[:,5:193]
        trainingAtters = numpy.concatenate((trainingOrdinalClean, trainingCategorial),1)
#         trainingAtters = numpy.concatenate((trainingOrdinal, trainingCategorial),1)
        trainingLabels = trainingPre[:,193]
           
        testingPre = allData[trainingSize:trainingSize+testingSize-1,:]
        testingOrdinal = testingPre[:,0:5]
        testingOrdinalClean = scaler.transform(testingOrdinal)
        testingCategorial = testingPre[:,5:193]
        testingAtters = numpy.concatenate((testingOrdinalClean, testingCategorial),1)
        testingLabels = testingPre[:,193]
         
        holdoutPre = allData[trainingSize+testingSize:allDataSize,:]
        holdoutOrdinal = holdoutPre[:,0:5]
        holdoutOrdinalClean = scaler.transform(holdoutOrdinal)
        holdoutCategorial = holdoutPre[:,5:193]
        holdoutAtters = numpy.concatenate((holdoutOrdinalClean, holdoutCategorial),1)
        holdoutLabels = holdoutPre[:,193]
         
        learner = LogisticRegression()
        
        result = buildThresholdCurve(learner, trainingAtters, trainingLabels, testingAtters, testingLabels)
        threshold = result['bestThreshold']
        y_prob = learner.fit(trainingAtters, trainingLabels).predict_proba(holdoutAtters)[:,1]
        holdoutLabels = holdoutLabels > 0 
        y_pred = y_prob >= threshold
        tp_array = (holdoutLabels == y_pred) & (y_pred == True)
        fp_array = (holdoutLabels != y_pred) & (y_pred == True)
        tn_array = (holdoutLabels == y_pred) & (y_pred == False)
        fn_array = (holdoutLabels != y_pred) & (y_pred == False)
        tp = float(numpy.sum(tp_array))
        fp = float(numpy.sum(fp_array))
        tn = float(numpy.sum(tn_array))
        fn = float(numpy.sum(fn_array))
        recall = tp/(tp+fn)
        if tp + fp > 0:
            precision = tp/(tp+fp)
        else:
            precision = 1
        if tn+fn > 0:
            npv = tn/(tn+fn)
        else:
            npv = 0
        if tn + fp > 0:
            specificity = tn/(tn+fp) #true negative rate
        else:
            specificity = 0
        if fp + tn > 0:
            fpr = fp/(fp+tn) #false positive rate, (1-specificity), fall-out
        else:
            fpr = 0
        fnr = fn/(fn+tp)
        acc = (tp+tn)/(tp+fp+tn+fn)    
        loss = float((holdoutLabels != y_pred).sum())/float(holdoutPre.shape[0])
        baseLoss = float(numpy.sum(holdoutLabels)) / float(holdoutLabels.size)
        print str(sampleProportion) + ', loss: ' + str(loss)
        csvwriter.writerow([sampleProportion, acc, loss, baseLoss, tp, tn, fp, fn])
#         print 'accuracy is ' + str(acc) + ', loss: ' + str(loss) + ', base-loss: ' + str(baseLoss) + ', tp: ' + str(tp) + ', tn: ' + str(tn) + ', fp: ' + str(fp) + ', fn: ' + str(fn)