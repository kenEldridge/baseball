import numpy, csv
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
import learner

class Result:
    def __init__(self):
        pass

colLabels = None
with open("../data.csv", 'r') as csvfile:
    data = csv.reader(csvfile, delimiter=',', quotechar='"')
    iterdata = iter(data)
    for row in iterdata:
        colLabels = row[1:len(row) - 1]
        break

#previous version were ommitiing the first column which is age.  Age should perhaps be ommited deliberately 
#because and age of 0 could indicate that a player isn't as important or remembered.  if age is missing 
#doesn't that tell us something we shouldn't know.
allData = numpy.loadtxt(open("../data.csv","rb"),delimiter=",",skiprows=1)

with open('learningCurve.csv', 'wb') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(['sampleProportion', 'accuracy', 'loss', 'base-loss', 'tp', 'tn', 'fp', 'fn'])
    for sampleProportion in numpy.arange(0.1,1.0,0.1):
        allDataSize = float(allData.shape[0])
        trainingSize = int( round( allDataSize * sampleProportion ) )
        remainder = allDataSize - trainingSize
        testingSize = round( remainder * .5 )
        holdoutSize = testingSize
        while (trainingSize + testingSize + holdoutSize) != allDataSize:
            holdoutSize =  holdoutSize - 1
        
        parameterCount = allData.shape[0]
        
                #randomize labels
#         randomLabels = numpy.empty((allData.shape[0],))
#         for index, item in enumerate(randomLabels):
#             randomLabels[index] = random.randint(0, 1)
#         allData[:,193] = randomLabels
        
        trainingPre = allData[0:trainingSize-1,:]
        trainingOrdinal = trainingPre[:,0:5]
        scaler = preprocessing.StandardScaler().fit(trainingOrdinal)
        trainingOrdinalClean = scaler.transform(trainingOrdinal)
        trainingCategorial = trainingPre[:,5:193]
        trainingAtters = numpy.concatenate((trainingOrdinalClean, trainingCategorial),1)
#         trainingAtters = numpy.concatenate((trainingOrdinal, trainingCategorial),1)
        trainingLabels = trainingPre[:,193]
           
        testingPre = allData[trainingSize:trainingSize+testingSize-1,:]
        testingOrdinal = testingPre[:,0:5]
        testingOrdinalClean = scaler.transform(testingOrdinal)
        testingCategorial = testingPre[:,5:193]
        testingAtters = numpy.concatenate((testingOrdinalClean, testingCategorial),1)
        testingLabels = testingPre[:,193]
         
        holdoutPre = allData[trainingSize+testingSize:allDataSize,:]
        holdoutOrdinal = holdoutPre[:,0:5]
        holdoutOrdinalClean = scaler.transform(holdoutOrdinal)
        holdoutCategorial = holdoutPre[:,5:193]
        holdoutAtters = numpy.concatenate((holdoutOrdinalClean, holdoutCategorial),1)
        holdoutLabels = holdoutPre[:,193]
        
        # gnb.fit(trainingAtters, trainingLabels).classes_  #find out the order of labels in gnb 
#         gnb = GaussianNB()
        rfc = RandomForestClassifier(n_estimators=100, min_samples_split=15, max_depth=15, min_samples_leaf=15)
        
        result = learner.buildThresholdCurve(rfc, trainingAtters, trainingLabels, testingAtters, testingLabels)
        threshold = result['bestThreshold']
        y_prob = rfc.fit(trainingAtters, trainingLabels).predict_proba(holdoutAtters)[:,1]
        featureRank = rfc.fit(trainingAtters, trainingLabels).feature_importances_
        holdoutLabels = holdoutLabels > 0 
        y_pred = y_prob >= threshold
        tp_array = (holdoutLabels == y_pred) & (y_pred == True)
        fp_array = (holdoutLabels != y_pred) & (y_pred == True)
        tn_array = (holdoutLabels == y_pred) & (y_pred == False)
        fn_array = (holdoutLabels != y_pred) & (y_pred == False)
        tp = float(numpy.sum(tp_array))
        fp = float(numpy.sum(fp_array))
        tn = float(numpy.sum(tn_array))
        fn = float(numpy.sum(fn_array))
        recall = tp/(tp+fn)
        if tp + fp > 0:
            precision = tp/(tp+fp)
        else:
            precision = 1
        if tn+fn > 0:
            npv = tn/(tn+fn)
        else:
            npv = 0
        if tn + fp > 0:
            specificity = tn/(tn+fp) #true negative rate
        else:
            specificity = 0
        if fp + tn > 0:
            fpr = fp/(fp+tn) #false positive rate, (1-specificity), fall-out
        else:
            fpr = 0
        fnr = fn/(fn+tp)
        acc = (tp+tn)/(tp+fp+tn+fn)    
        loss = float((holdoutLabels != y_pred).sum())/float(holdoutPre.shape[0])
        baseLoss = float(numpy.sum(holdoutLabels)) / float(holdoutLabels.size)
        print str(sampleProportion) + ', loss: ' + str(loss)
        csvwriter.writerow([sampleProportion, acc, loss, baseLoss, tp, tn, fp, fn])

with open('labelSignificance.csv', 'wb') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(['Label', 'Significance'])
    for index, item in enumerate(colLabels):
        csvwriter.writerow([item, featureRank[index]])
