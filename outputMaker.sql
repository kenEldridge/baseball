/*This is the query that generates the csv output */
SELECT distinct m.playerID AS id, m.height AS height, Age(m.birthDay, m.birthMonth, m.birthYear, m.debut) AS age,
					c.cIndex AS birth_location, m.bats+0 AS bats, m.throws+0 AS throws, Year(m.debut) AS debutYear, Month(m.debut) AS debutMonth, m.yearMax - m.yearMin AS years_school,
					PositionPlayed(a.G_p, a.G_c, a.G_1b, a.G_2b, a.G_3b, a.G_ss, a.G_lf, a.G_cf, a.G_rf, a.G_of, a.G_dh, a.G_ph, a.G_pr) AS position_played,
					NumPositionPlayed(a.G_p, a.G_c, a.G_1b, a.G_2b, a.G_3b, a.G_ss, a.G_lf, a.G_cf, a.G_rf, a.G_of, a.G_dh, a.G_ph, a.G_pr) AS number_positions,
					m.schoolLocation AS school_location, m.yearN as nth_year, o.obs as obs1, o2.obs as obs4, (SELECT max( l_sub.good ) FROM Label l_sub
																								WHERE l_sub.playerID = m.playerID
																									AND l_sub.yearID = m.yearN) as good
					    FROM Countries c, appearances a, obs o, midpoint m LEFT OUTER JOIN obs o2
							ON m.playerID = o2.playerID and m.yearN = o2.yearID
					        WHERE (m.birthCountry = c.country OR m.birthState = c.country)
					            AND c.cIndex != 65
								AND m.playerID = a.playerID
								AND a.yearID = Year(m.debut)
					            AND m.playerID = o.playerID
                                AND a.yearID = o.yearID
									ORDER BY m.playerID;
