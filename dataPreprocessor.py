from sklearn import preprocessing
import numpy as np
import csv

colLabels = None
with open("1955_2005_withLabelsYear_v2.csv", 'r') as csvfile:
    data = csv.reader(csvfile, delimiter=',', quotechar='"')
    iterdata = iter(data)
    for row in iterdata:
        colLabels = row[1:len(row) - 1]
        break

# colLabels = colLabels[0,:]
rawData = np.loadtxt(open("1955_2005_withLabelsYear_v2.csv","rb"),delimiter=",",skiprows=1,usecols=range(1,14))
ordered1 = rawData[:,0:2]
orderedColLabels1 = colLabels[0:2]
ordered2 = rawData[:,7:8]
orderedColLabels2 = colLabels[7:8]
ordered3 = rawData[:,9:10]
orderedColLabels3 = colLabels[9:10]
ordered4 = rawData[:,11:12]
orderedColLabels4 = colLabels[11:12]
ordered = np.concatenate((ordered1, ordered2, ordered3, ordered4), 1)
orderedColLabels = np.concatenate((orderedColLabels1, orderedColLabels2, orderedColLabels3, orderedColLabels4), 1)

categorial1 = rawData[:,2:7]
categorialColLabels1 = colLabels[2:7]
categorial2 = rawData[:,8:9]
categorialColLabels2 = colLabels[8:9]
categorial3 = rawData[:,10:11]
categorialColLabels3 = colLabels[10:11]
categorial = np.concatenate((categorial1, categorial2, categorial3), 1)
categorialColLabels = np.concatenate((categorialColLabels1, categorialColLabels2, categorialColLabels3), 1)

labels = rawData[:,12:13]
#Standardization, or mean removal and variance scaling
# X = np.array([[ 1., -1.,  2.], [ 2.,  0.,  0.], [ 0.,  1., -1.]])
# print 'X: ' + str(X)
# X_scaled = preprocessing.scale(X)
# print 'X_scaled: ' + str(X_scaled)
# print 'X_scaled.mean(axis=0): ' + str(X_scaled.mean(axis=0))
# print 'X_scaled.std(axis=0): ' + str(X_scaled.std(axis=0))

#build with training data
# scaler = preprocessing.StandardScaler().fit(X)
# print 'scaler: ' + str(scaler)
# print 'scaler.mean_: ' + str(scaler.mean_)                                      
# print 'scaler.std_: ' + str(scaler.std_)               

#apply to test data
# scaler.transform(X) 

#Normalization
# print 'X: ' + str(X)
# X_normalized = preprocessing.normalize(X, norm='l2')
# print 'X_normalized: ' + str(X_normalized)
# 
# normalizer = preprocessing.Normalizer().fit(X)  # fit does nothing
# print 'normalizer: ' + str(normalizer)
# 
# print 'normalizer.transform(X): ' + str(normalizer.transform(X))

#one-hot encodingenc = preprocessing.OneHotEncoder()
enc = preprocessing.OneHotEncoder()
categorialFit = enc.fit(categorial)
print 'enc.fit: ' + str(categorialFit)
categorialTransformed = categorialFit.transform(categorial).toarray()
featureIndecies = categorialFit.feature_indices_

def findLabel(colIndex):
    label = None
    for ent_ind, ent in enumerate(featureIndecies):
        if colIndex >= ent and ent_ind + 1 < featureIndecies.shape[0] and colIndex <= featureIndecies[ent_ind + 1]:
            label = categorialColLabels[ent_ind]
            break
    return label

cols = categorialTransformed[0,:]
categoricalColLabels = np.empty(cols.shape[0] + 1, dtype=object)
for index, val in enumerate(categorialFit.active_features_):
    categoricalColLabels[index] = findLabel(val)

columnLabels = np.append(orderedColLabels, categoricalColLabels)
transformedData = np.concatenate((ordered, categorialTransformed, labels),1)
transformedDataStr = transformedData.astype(str)
transformedDataStr = np.insert(transformedDataStr, 0, columnLabels, axis=0)
print transformedDataStr.shape
np.savetxt("data.csv", transformedDataStr, delimiter=",", fmt="%s")

