from sklearn import preprocessing
import numpy as np

#Standardization, or mean removal and variance scaling
X = np.array([[ 1., -1.,  2.], [ 2.,  0.,  0.], [ 0.,  1., -1.]])
print 'X: ' + str(X)
X_scaled = preprocessing.scale(X)
print 'X_scaled: ' + str(X_scaled)
print 'X_scaled.mean(axis=0): ' + str(X_scaled.mean(axis=0))
print 'X_scaled.std(axis=0): ' + str(X_scaled.std(axis=0))

#build with training data
scaler = preprocessing.StandardScaler().fit(X)
print 'scaler: ' + str(scaler)
print 'scaler.mean_: ' + str(scaler.mean_)                                      
print 'scaler.std_: ' + str(scaler.std_)               

#apply to test data
scaler.transform(X) 

#Normalization
print 'X: ' + str(X)
X_normalized = preprocessing.normalize(X, norm='l2')
print 'X_normalized: ' + str(X_normalized)

normalizer = preprocessing.Normalizer().fit(X)  # fit does nothing
print 'normalizer: ' + str(normalizer)

print 'normalizer.transform(X): ' + str(normalizer.transform(X))

#one-hot encodingenc = preprocessing.OneHotEncoder()
enc = preprocessing.OneHotEncoder()
print 'enc.fit: ' + str(enc.fit([[0, 0, 3], [1, 1, 0], [0, 2, 1], [1, 0, 2]]))  
print 'enc.transform: ' + str(enc.transform([[0, 1, 3]]).toarray())
